package com.wlhlearn.spring01.versionthree;

import com.wlhlearn.spring01.myannotation.WLHRequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @Author: wlh
 * @Date: 2019/3/28 17:03
 * @Version 1.0
 * @despricate:  在springmvc 的源码中 handmapping 是一个list,里面存储的自定义处理类  现在进行改造
 *
 */
public class Handler {

    private String url ;

     private  Object controller ;

     private Method  method ;

     private Map<String,Integer> paramIndexMapping ;   // 参数列表  名字作为key 所在序号为value

    private Class<?>[]  paramsTypes ;


    public Handler(Object controller, Method method, String url) {
        this.controller = controller;
        this.method = method;
        this.url = url;
        paramsTypes=method.getParameterTypes();
        paramIndexMapping=new HashMap<String,Integer>();
        //  参数的初始化
        putParamIntoMap(method);

    }

    private void putParamIntoMap(Method method) {
        // 因为一个参数可以有多个注解， 同时一个方法中可以有多个参数  所以是一个二维数组
        Annotation[][] pa=method.getParameterAnnotations();
        for (int i=0 ;i<pa.length;i++){
            for (Annotation p2:pa[i]){
               if(p2 instanceof WLHRequestParam){
                   String paramName=((WLHRequestParam) p2).value();
                   if(!"".equals(paramName)){
                       paramIndexMapping.put(paramName,i);
                   }
               }
            }
        }

        Class<?>[] paramTypes=method.getParameterTypes();
        for (int i=0 ;i<paramTypes.length;i++){
            Class<?> tmp=paramTypes[i] ;
            if(tmp== HttpServletRequest.class || tmp== HttpServletResponse.class){
                paramIndexMapping.put(tmp.getName(),i);
            }

        }

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Class<?>[] getParamsTypes() {
        return paramsTypes;
    }

    public Map<String, Integer> getParamIndexMapping() {
        return paramIndexMapping;
    }
}
