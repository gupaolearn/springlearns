package com.wlhlearn.spring01.myannotation;

import java.lang.annotation.*;

/**
 * @Author: wlh
 * @Date: 2019/3/28 10:03
 * @Version 1.0
 * @despricate:learn
 */

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WLHRequestMapping {

    String value()  default "";
}
