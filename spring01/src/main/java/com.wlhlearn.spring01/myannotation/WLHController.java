package com.wlhlearn.spring01.myannotation;

import java.lang.annotation.*;

/**
 * @Author: wlh
 * @Date: 2019/3/28 10:01
 * @Version 1.0
 * @despricate:learn
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WLHController {
    String value()  default "";
}
