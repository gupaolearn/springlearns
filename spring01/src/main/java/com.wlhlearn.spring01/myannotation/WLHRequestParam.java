package com.wlhlearn.spring01.myannotation;

import java.lang.annotation.*;

/**
 * @Author: wlh
 * @Date: 2019/3/28 10:04
 * @Version 1.0
 * @despricate:learn
 */

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WLHRequestParam {

    String value()  default "";
}
