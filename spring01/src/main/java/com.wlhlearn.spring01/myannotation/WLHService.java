package com.wlhlearn.spring01.myannotation;
/**
 * @Author: wlh
 * @Date: 2019/3/28 9:56
 * @Version 1.0
 * @despricate:learn
 */
import java.lang.annotation.* ;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WLHService {

    String value()  default "";
}
