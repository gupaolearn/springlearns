package com.wlhlearn.spring01.myannotation;

import java.lang.annotation.*;

/**
 * @Author: wlh
 * @Date: 2019/3/28 9:59
 * @Version 1.0
 * @despricate:learn
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WLHAutowired {
    String value()  default "";
}
