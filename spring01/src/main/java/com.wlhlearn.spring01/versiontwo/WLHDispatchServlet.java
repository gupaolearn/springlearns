package com.wlhlearn.spring01.versiontwo;

import com.wlhlearn.spring01.myannotation.WLHAutowired;
import com.wlhlearn.spring01.myannotation.WLHController;
import com.wlhlearn.spring01.myannotation.WLHRequestMapping;
import com.wlhlearn.spring01.myannotation.WLHService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;

/**
 * @Author: wlh
 * @Date: 2019/3/28 9:19
 * @Version 1.0
 * @despricate:  springmvc 的简易实现版本
 */
public class WLHDispatchServlet extends HttpServlet {

    private Properties properties = new Properties();

    private List<String> classNamesAll = new ArrayList<String>();

    private Map<String, Object> ioc = new HashMap<String, Object>();

    private Map<String, Method> handmapping = new HashMap<String, Method>();

    @Override
    public void init(ServletConfig config) throws ServletException {
        // 1  加载配置文件
        doLoadConfig(config.getInitParameter("contextConfigLocation"));
        //  2  扫描相关的类文件
        doScanClass(properties.getProperty("scanPackage"));
        //  3 初始化扫描到的类  并将它们放入到IOC容器中
        try {
            doInitClass();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        //  4  完成依赖注入
        doAutowired();
        //   5   初始化handmapping
        doInitHandMapping();
    }

    private void doInitHandMapping() {
        if (ioc.isEmpty()) return;
        for (Map.Entry<String, Object> entry : ioc.entrySet()) {

            Class<?> clazz = entry.getValue().getClass();
            if (!clazz.isAnnotationPresent(WLHController.class)) {
                continue;
            }
            String beanUrl="";
            if(clazz.isAnnotationPresent(WLHRequestMapping.class)){
                WLHRequestMapping requestMapping=clazz.getAnnotation(WLHRequestMapping.class);
                beanUrl=requestMapping.value();
            }

            //  默认获取所有的public 方法
            for (Method method:clazz.getMethods()){
                if(!clazz.isAnnotationPresent(WLHRequestMapping.class)){
                    continue;
                }
                if(method.isAnnotationPresent(WLHRequestMapping.class)){
                    WLHRequestMapping requestMapping=method.getAnnotation(WLHRequestMapping.class);
                    if(null==requestMapping|| "".equals(requestMapping)) return;
                    String url=beanUrl+requestMapping.value().trim();
                    handmapping.put(url,method);
                    System.out.println("for method:"+url +"   , "+method);
                }
            }

        }

    }

    private void doAutowired() {
        if (ioc.isEmpty()) return;

        for (Map.Entry<String, Object> entry : ioc.entrySet()) {

            Field[] fields = entry.getValue().getClass().getDeclaredFields();
            for (Field field : fields) {
                if (!field.isAnnotationPresent(WLHAutowired.class)) {
                    continue;
                }

                WLHAutowired autowired = field.getAnnotation(WLHAutowired.class);
                String beanName = autowired.value().trim();
                if ("".equals(beanName)) {
                    beanName = field.getType().getName();
                }
                field.setAccessible(true);

                try {
                    field.set(entry.getValue(), ioc.get(beanName));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    // 将已知的所有的classNames 实例化 并放入IOC容器中
    private void doInitClass() throws IllegalAccessException, InstantiationException {
        if (classNamesAll == null || classNamesAll.size() == 0) return;
        try {
            for (String classname : classNamesAll) {

                Class<?> clazz = Class.forName(classname);
                // 什么样的类 才需要初始化呢？  带有申明标签的  如@controller @Service .....
                if (clazz.isAnnotationPresent(WLHController.class)) {
                    Object instance = clazz.newInstance();
                    String beanName = toLowerFirstCase(clazz.getSimpleName());
                    ioc.put(beanName, instance);
                } else if (clazz.isAnnotationPresent(WLHService.class)) {
                    WLHService service = clazz.getAnnotation(WLHService.class);
                    String serverName = service.value();
                    if ("".equals(serverName.trim())) {
                        serverName = toLowerFirstCase(clazz.getSimpleName());
                    }

                    Object instance = clazz.newInstance();
                    ioc.put(serverName, instance);

                    // 根据类型自动赋值
                    for (Class<?> c : clazz.getInterfaces()) {
                        if (ioc.containsKey(c.getName())) {
                            //throw new Exception("the  method" + c.getName() + "exist");
                        }
                        ioc.put(c.getName(), instance);
                    }
                } else {
                    continue;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private String toLowerFirstCase(String simpleName) {
        char[] chars = simpleName.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    //  扫描所配置的包路径下的所有的类
    private void doScanClass(String scanPackage) {

        URL url = this.getClass().getClassLoader().getResource("/" + scanPackage.replaceAll("\\.", "/"));
        File file = new File(url.getFile());
        // 循环判断当前文件路径下的所有文件
        for (File f : file.listFiles()) {
            if (f.isDirectory()) {
                doScanClass(scanPackage + "." + f.getName());
            } else {
                if (!f.getName().endsWith(".class")) continue;
                String classNames = scanPackage + "." + f.getName().replaceAll(".class", "");
                //  将获取到的class 路径放入存储的list中
                classNamesAll.add(classNames);
            }
        }


    }

    //  加载对应的配置文件
    private void doLoadConfig(String contextConfigLocation) {
        //  直接从类路径下找到sping 主配置文件所在的位置
        //  以输入流的形式读取出来 放到properties 中    完成内容从文件到内存的转换
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation);
        try {
            properties.load(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           doDispatchNow(req,resp);
    }

    private void doDispatchNow(HttpServletRequest req, HttpServletResponse resp) {

        String url=req.getRequestURI();
        System.out.println(url);

        String contextPath=req.getContextPath() ;
        url=url.replace(contextPath,"").replaceAll("/+","/");
        if(!this.handmapping.containsKey(url)){
            try {
                resp.getWriter().write("404 not found");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Method method=this.handmapping.get(url);
        Map<String,String[]> params=req.getParameterMap() ;
        String beanName=toLowerFirstCase(method.getDeclaringClass().getSimpleName());
        try {
            method.invoke(this.ioc.get(beanName),new Object[]{req,resp,params.get("name")[0]});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }
}
