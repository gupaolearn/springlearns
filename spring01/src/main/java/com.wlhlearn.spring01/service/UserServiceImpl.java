package  com.wlhlearn.spring01.service;


import com.wlhlearn.spring01.myannotation.WLHService;

/**
 * @Author: wlh
 * @Date: 2019/3/28 10:07
 * @Version 1.0
 * @despricate:learn
 */


@WLHService
public class UserServiceImpl implements IUserService {
    public void insertUser() {
        System.out.println("insert user");
    }
}
