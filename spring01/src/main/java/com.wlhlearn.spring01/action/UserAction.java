package com.wlhlearn.spring01.action;
import com.wlhlearn.spring01.myannotation.WLHAutowired;
import com.wlhlearn.spring01.myannotation.WLHController;
import com.wlhlearn.spring01.myannotation.WLHRequestMapping;
import com.wlhlearn.spring01.myannotation.WLHRequestParam;
import com.wlhlearn.spring01.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: wlh
 * @Date: 2019/3/28 10:09
 * @Version 1.0
 * @despricate:learn
 */


@WLHController
@WLHRequestMapping("/user")
public class UserAction   {

    @WLHAutowired
    private IUserService userService ;


    @WLHRequestMapping("/insert")
    public void  insertUser(HttpServletRequest request,HttpServletResponse response, @WLHRequestParam("name")String name){
        System.out.println(name);
        userService.insertUser();
        try {
            response.getWriter().write("hello "+name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
